#
# Solves the distance constraint variational problem
#

# Find the l and r such that |l - r| = d with a minimal d(l, l_) and d(r, r_)

L := 1/2 * ((l - l_)^2 + (r - r_)^2) + g/4 * ((l - r)^2 - d^2):

# Solution to verify, writting |l_ - r_| = d_

sl := {
 l = (l_ + r_) / 2 + d / (2 * d_) * (l_ - r_),
 r = (l_ + r_) / 2 + d / (2 * d_) * (r_ - l_)
}:

ok_lr := evalb({0} = simplify(subs(sl, g = d_ / d - 1, {diff(L, l), diff(L, r), diff(L, g)}), {l_ - r_ = d_}));

# Since a linear solution, this equation holds for l, l_, r, r_ vectors.

#
# Geometry of the |l - r| = d hypersurface in R^d x R^d
#

with(LinearAlgebra):
with(ArrayTools):

# Quadratic form of (l_x - r_x)^2 + (l_y - r_y)^2 = [l_x, l_y, r_x, r_y]^T Q [l_x, l_y, r_x, r_y] = d^2, Q = R^T L R

d := 2:

l := Vector(d, i -> cat('l_', i)):
r := Vector(d, i -> cat('r_', i)):
lr := Concatenate(1, l, r):

Zr := Matrix(d, d, (i, j) -> 0);
Un := IdentityMatrix(d, d);

Q_ := Matrix(2 * d, 2 * d, (i, j) -> `if`(i = j, 1, `if`(i = j - d or j = i - d, -1, 0))):
Q  := Concatenate(2,
	Concatenate(1,  Un, -Un),
     	Concatenate(1, -Un,  Un)):

z := expand(Transpose(l - r) . (l - r) - Transpose(lr) . Q . lr);

R_  := Matrix(2 * d, 2 * d, (i, j) -> `if`(i = j or i - d = j, 1, `if`(i = j - d, -1, 0))) / sqrt(2):
#R := Concatenate(2,
#	Concatenate(1,  Un, -Un),
#     	Concatenate(1,  Un,  Un)) / sqrt(2):
R := R_: # there seems to be a caveat in ArrayTools[Concatenate] 
	
L_ := Matrix(2 * d, 2 * d, (i, j) -> `if`(i <= d and i = j, 2, 0)):
L  := Concatenate(2,
	Concatenate(1, 2 * Un, Zr),
     	Concatenate(1,     Zr, Zr)):

z := Norm(Transpose(R) . L . R - Q) + Norm(Q - Q_) + Norm(R - R_) + Norm(L - L_);
 
# => hypersphera of dimension d and radius d / sqrt(2)
