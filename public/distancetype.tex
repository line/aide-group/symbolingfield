%
% This file is used in  
%   https://gitlab.inria.fr/line/aide-group/aide/-/blob/master/etc/tex_to_pdf.sh
% and command are documented in http://aide-line.inria.fr/build/www/etc.html#.tex_to_pdf
%

% Defines a minimal layout for pdf display

\documentclass[a4paper,12pt,landscape,pdftoolbar=false,pdfmenubar=false]{article}
\pagestyle{empty} 
\topmargin 0cm \oddsidemargin 0cm \evensidemargin 0cm 
\setlength{\parindent}{0in} 
\setlength{\parskip}{3mm} 
\usepackage[margin=2cm]{geometry}

% Here are the used packages

\usepackage[utf8]{inputenc}
\DeclareUnicodeCharacter{00B0}{\textsuperscript{o}}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}\usepackage{amssymb}\usepackage{amsfonts}
\usepackage{array}
\usepackage{times}
\usepackage{color}
%\usepackage{algpseudocode}\usepackage{algorithm}
\usepackage{listings}

% Here is tha macro for conditional compilation

\def\aidebuild {}

% Here are some used commands

\newcommand{\deq}{\stackrel {\rm def}{=}} 
\newcommand{\eqline}[1]{~\vspace{0.3cm}\\\centerline{$#1$}\vspace{0.3cm}\\}
\newcommand{\tab}{\hphantom{6mm}}
\newcommand{\hhref}[1]{\href{#1}{#1}}

% Here are some homemade commands to define minimal slides

\newcommand{\slide}[1]{\clearpage\fbox{\parbox[t][16.5cm][t]{\textwidth}{\Huge #1}}\newpage}
\newcommand{\stitle}[1]{~\vspace{1cm}\\\centerline{\fontsize{40}{50}\selectfont \bf #1}\vspace{0.5cm}\\}
\newcommand{\sright}[1]{\begin{flushright}#1\tab\end{flushright}}
\newcommand{\scenter}[1]{\begin{center}#1\end{center}}
\newcommand{\stwo}[4]{\begin{tabular}{ll}\parbox{#1\textwidth}{#3}&\parbox{#2\textwidth}{#4}\end{tabular}}

\begin{document}
\subsection*{Projection of an equi-distance manifold}

Given two points $\bar{\bf l}$ and $\bar{\bf r}$ in ${\cal R}^d$ we look for their projection ${\bf l}$ and ${\bf r}$ on the manifold of dimension $d-1$ defined by $\|{\bf l} - \bar{\bf r}\| \;\diamond\; d$, with $\diamond \in \{=, \le, \ge\}$, i.e.,, of a predefined inter-distance and such that $d({\bf l}, \bar{\bf l})$ and $d({\bf r}, \bar{\bf r})$ is minimal.

This may be written in variational form:
\eqline{\min_{{\bf l},{\bf r}} {\cal L}, \;\;\; {\cal L} \deq \frac{1}{2} \, \left(\|{\bf l} - \bar{\bf l}\|^2 + \|{\bf r} - \bar{\bf r}\|^2\right) + \frac{\lambda}{2} \, \left( \|{\bf l} - \bar{\bf r}\|^2 \;\diamond\; d^2 \right),}
which normal equations yield:
\eqline{\begin{array}{rclcl}
 0 &=& \partial_{\bf l}{\cal L} &=& ({\bf l} - \bar{\bf l}) + \lambda \, ({\bf l} - \bar{\bf r}) \\
 0 &=& \partial_{\bf r}{\cal L} &=& ({\bf r} - \bar{\bf r}) + \lambda \, ({\bf r} - \bar{\bf l}) \\
 0 &=& \partial_{\lambda}{\cal L} &=& \|{\bf l} - \bar{\bf r}\|^2 \diamond d^2 \\
\end{array}}
and it is easy to verify that the solution writes:
\eqline{\begin{array}{rcl}
 {\bf l} &=& \frac{\bar{\bf l} + \bar{\bf r}}{2} +
    \frac{d}{2\,\|\bar{\bf l} - \bar{\bf r}\|} \, (\bar{\bf l} - \bar{\bf r}) \\
 {\bf r} &=& \frac{\bar{\bf l} + \bar{\bf r}}{2} +
    \frac{d}{2\,\|\bar{\bf l} - \bar{\bf r}\|} \, (\bar{\bf r} - \bar{\bf l} )\\
\end{array}}
the 1st term allowing to be as close as possible to the original values and the second term to fruitful the distance requirement, as verified from a small piece of computer algebra code\footnote{\url{https://gitlab.inria.fr/line/aide-group/symbolingfield/-/raw/master/src/distancetype.mpl?ref_type=heads}}. In the scalar case, it simplifies to:
\eqline{\begin{array}{rcl}
 l &=& \frac{\bar{l} + \bar{r}}{2} + \mbox{sign}(\bar{l} - \bar{r}) \, \frac{d}{2} \\
 r &=& \frac{\bar{l} + \bar{r}}{2} + \mbox{sign}(\bar{r} - \bar{l}) \, \frac{d}{2} \\
\end{array}}

\newpage

At a geometric level, in the ${\cal R}^{2\,d} = {\cal R}^d \times {\cal R}^d$ space, the hyper-surface $\|{\bf l} - \bar{\bf r}\|^2 = d^2$ is the hypersphere\footnote{Since $\|{\bf l} - \bar{\bf r}\|^2 = d^2$ writes in matrix form:
\eqline{
  {\bf l}^T \, {\bf l} - 2 \, {\bf l}^T \, {\bf r} + {\bf r}^T \, {\bf r} = 
  \left( \begin{array}{cc} {\bf l}^T & {\bf r}^T \end{array} \right) \,
  \underbrace{\left( \begin{array}{cc} {\bf I} & -{\bf I} \\ -{\bf I} & {\bf I} \\\end{array} \right)}_{\deq {\bf Q}} \,
  \left( \begin{array}{c} {\bf l} \\ {\bf r} \end{array} \right) = d^2}
with:
\eqline{{\bf Q} = {\bf R}^T \, {\bf L} \, {\bf R},
   {\bf R} \deq \frac{1}{\sqrt{2}} \left( \begin{array}{cc} {\bf I} & -{\bf I} \\ {\bf I} & {\bf I} \\\end{array} \right),
     {\bf L} \deq \left( \begin{array}{cc} 2 \, {\bf I} & {\bf 0} \\ {\bf 0} & {\bf 0} \\\end{array} \right)}
using ${\bf R}$, with ${\bf R}^T \, {\bf R}$, thus a rotation, as change of coordinates, we are left a quadratic form defined by ${\bf L}$, thus an hypersphere in a linear subspace, as expected.} of radius $d/\sqrt{2}$ in the linear subspace defined by ${\bf l} + {\bf r} = 0$, while it vanishes in the orthogonal linear subspace defined by ${\bf l} = {\bf r}$.

This very simple geometry is interesting because it allows one to see that ``outside'', i.e., for $\|{\bf l} - \bar{\bf r}\| \;>\; d$ the hyper-surface is convex.


\end{document}
