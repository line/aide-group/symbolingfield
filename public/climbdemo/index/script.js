//
// Canvas setup
//

const canvas = document.getElementById("canvas");
const canvas_context = canvas.getContext("2d");
canvas.width = window.innerWidth - canvas.offsetLeft;
canvas.height = window.innerHeight - 50;

// Initial body state position
let initPos = { x: canvas.width / 2, y: canvas.height - 100};

//
// Wall specification and basic tool
//

let wall = [
  // -- position of the hold --
  { x: initPos.x, y: initPos.y}
]
// ... by contract the last wall position is the goal

// Draws the wall

function drawWall() {
  // Clears the canvas
  canvas_context.clearRect(0, 0, canvas.width, canvas.height);
  wall.forEach((position, index) => {
    // Draws the hold circle
    let final = index == wall.length - 1;
    canvas_context.fillStyle = final ? "green" : "blue";
    let hold = new Path2D();
    hold.arc(position.x, canvas.height - position.y, 10, 0, 2 * Math.PI);
    canvas_context.fill(hold);
    // Draws the hold index
    canvas_context.font = "24px serif";
    canvas_context.fillStyle = "black";
    canvas_context.fillText("" + index, position.x + 10, canvas.height - position.y);
  });
}

//
// State specification and basic tool
//

let state = {
  //        -- position --    -- wall hold index --
  limbs: {
    hleft:  { x: null, y: null, h: null }, // high left hand
    hright: { x: null, y: null, h: null }, // high right hand
    lleft:  { x: null, y: null, h: null }, // low left leg or foot
    lright: { x: null, y: null, h: null }, // low right leg or foot
  },
  body: { x: initPos.x, y: initPos.y }
};

// Sets the state from a path step and draws the climber

function setStateFromPathStep(step) {
  // Sets the limbs and body from wall hold indexes
  function setStateFromWallHolds() {
    // Sets the limb positions from wall hold positions
    for (let i in state.limbs) {
      if (state.limbs[i].h != null) {
	if (0 <= state.limbs[i].h && state.limbs[i].h < wall.length) {
	  state.limbs[i].x = wall[state.limbs[i].h].x;
	  state.limbs[i].y = canvas.height - wall[state.limbs[i].h].y;
	} else
	  alertonerror("setStateFromWallHold IndexError: { wall.length: " + wall.length+" }");
      }
    }
    // Sets body position as the limbs gravity center
    {
      let x0 = 0, y0 = 0;
      for (let i in state.limbs) {
	x0 += state.limbs[i].x;
	y0 += state.limbs[i].y;
      }
      state.body.x = x0 / 4;
      state.body.y = y0 / 4;
    }
  }
  // Reports the wall hold indexes
  for (let i in state.limbs) {
    if(step[i] != null) {
      state.limbs[i].h = step[i];
    }
  }
  setStateFromWallHolds();
  checkState();
  drawWall();
  drawState();
}

// Sets the limbs from the body position and draws the climber

function setStateFromBody() {
  const relative_position = {
    hleft:  { dx: -70, dy: -20}, 
    hright: { dx: 70,  dy: -20}, 
    lleft:  { dx: -50, dy: 80}, 
    lright: { dx: 50,  dy: 80}};
  for (let i in relative_position) {
    state.limbs[i].x = state.body.x + relative_position[i].dx;
    state.limbs[i].y = state.body.y + relative_position[i].dy;
    state.limbs[i].h = null;
  }
  checkState();
  drawWall();
  drawState();
}

const limb_max_distance = 200;

// Checks the state constraints

function checkState() {
  function d(lhs, rhs) {
    let dx = lhs.x - rhs.x, dy = lhs.y - rhs.y;
    return Math.sqrt(dx * dx + dy * dy);
  }
  let check =
      // Maximal distances
      (d(state.limbs.hleft, state.body) < limb_max_distance ? "" : "\t! hleft too long\n") +
      (d(state.limbs.hright, state.body) < limb_max_distance ? "" : "\t! hright too long\n") +
      (d(state.limbs.lleft, state.body) < limb_max_distance ? "" : "\t! lleft too long\n") +
      (d(state.limbs.lright, state.body) < limb_max_distance ? "" : "\t! lright too long\n") +
      // Hands above legs
      (state.limbs.hleft.y < state.limbs.lleft.y ? "" : "\t! hleft below lleft\n") +
      (state.limbs.hright.y < state.limbs.lright.y ? "" : "\t! hright below lright\n") +
      // Hand and leg left right orientation
      (state.limbs.hleft.x < state.limbs.hright.x ? "" : "\t! hleft and hright inverted\n") +
      (state.limbs.lleft.x < state.limbs.lright.x ? "" : "\t! lleft and lright inverted\n");
  if (check != "")
    alertonerror("checkState Error: \n"+check);
}

// Draws the climber

function drawState() {
  // Draws the limbs labels
  {
    canvas_context.font = "24px serif";
    canvas_context.fillStyle = "black";
    for (let i in state.limbs) {
      if (state.limbs[i].x != null && state.limbs[i].y != null) {
	canvas_context.fillText(i.substr(0, 2), state.limbs[i].x - 30, state.limbs[i].y);
      }  else
	alertonerror("drawState LimbPositionError:");
    }
  }
  // Checks if done, drawing in red or green
  {
    canvas_context.fillStyle = "red";
    let done = false;
    for (let i in state.limbs)
      if (state.limbs[i].h == wall.length - 1)
	canvas_context.fillStyle = "green";
  }
  // Draws the limbs extremities
  {
    for (let i in state.limbs) {
      if (state.limbs[i].x != null && state.limbs[i].y != null) {
	if(i[0] == 'h') {
	  let hand = new Path2D();
	  hand.ellipse(state.limbs[i].x, state.limbs[i].y, 10, 15, 0, 0, 2 * Math.PI);
	  hand.ellipse(state.limbs[i].x + 5, state.limbs[i].y + 3, 5, 15, (40 * Math.PI) / 180, 0, 2 * Math.PI);
	  canvas_context.fill(hand);
	} else {
	  let feet = new Path2D();
	  feet.ellipse(state.limbs[i].x, state.limbs[i].y, 15, 10, 0, 0, 2 * Math.PI);
	  feet.ellipse(state.limbs[i].x + 16, state.limbs[i].y, 5, 8, 0, 0, 2 * Math.PI);
	  canvas_context.fill(feet);
	}
      }
    }
  }
  // Draws the limbs links
  for (let i in state.limbs) {
    if (state.limbs[i].x != null && state.limbs[i].y != null) {
      canvas_context.beginPath();
      canvas_context.moveTo(state.limbs[i].x, state.limbs[i].y);
      canvas_context.lineTo(state.body.x, state.body.y);
      canvas_context.stroke();
    }
  }
  // Draws the body
  if (state.body.x != null && state.body.y != null) {
    let body = new Path2D();
    body.ellipse(state.body.x, state.body.y, 25, 30, 0, 0, 2 * Math.PI);
    canvas_context.fill(body);
    let head = new Path2D();
    head.arc(state.body.x, state.body.y - 35, 20, 0, 2 * Math.PI);
    canvas_context.fill(head);
  } else
     alertonerror("drawState BodyPositionError:");
  // Prints the position
  document.getElementById('coord').innerHTML = 'X = '+state.body.x+' Y = '+state.body.y;
}

//
// URL handler
//
// Usage: index.html?wall=${wall-hold-list}&initPos={x:***,y:***}&path=${path-sequence}

let urlParams = new URLSearchParams(window.location.search);

for (let [key, value] of urlParams) {
  switch (key) {
  case "wall": {
    wall = JSON.parse(value).wall;
    drawWall();
    break;
  }
  case "initPos": {
    initPos = JSON.parse(value);
    state.body = { x: initPos.x, y: initPos.y };
    setStateFromBody();
    break;
  }
  case "path" : {
    async function run() {
      // Endless loop on the path
      let path = JSON.parse(value).path;
      while(true) {
	state.body = { x: initPos.x, y: initPos.y };
	setStateFromBody();
	for(let i in path) {
	  await delay(1000);
	  setStateFromPathStep(path[i]);
	}
	await delay(2000);
      }}; run();
    break;
  }
  }
}

// Waits a gievn amount of milliseconds

function delay(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

// Reports an error

function alertonerror(message) {
  message += " state:\n"+ JSON.stringify(state, null, "  ") + "\n";
  document.getElementById('console').innerHTML = "<pre>" + message + "/<pre>";
}

