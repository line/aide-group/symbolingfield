#ifndef __symboling_InequalityType__
#define __symboling_InequalityType__

#include "Value.hpp"
#include "Type.hpp"
#include "NumericType.hpp"

namespace symboling {
  /**
   * @class InequalityType
   * @description Implements a type corresponding to a inequality constraint.
   * <a name="description"></a>
   * - It considers value pairs with `right > left` defined by a NumericType `ntype` of the form:
   * ```
   * {
   *  ../..
   *  left: ntype
   *  right: ntype
   * ../..
   * }
   * ```
   * thus a record with two `ntype` numerical values, where `left`, `right`, and `ntype` is user defined.
   * - This type corresponds to value for which `right - left = distance` which borders the `right - left > distance` region.
   *   - Derivation is available in [this note](./distancetype.pdf).
   *
   * <a name="specification"></a>
   * ## ValueType specification
   * <table>
   * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
   * - Returns a default value, if the underlying numeric type values are undefined.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
   * - Returns the closest value wich verifies the distance equality.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
   * - Returns the Euclidean distance between the current value.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
   * - The geodesic is calculated using the underlying numeric type precision increment.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
   * - Returns <tt>NAN</tt> no comparison possible for this type.
   * </td></tr>
   * <tr style='border: 1px solid black;'><th> Bounds           </th><td>
   * - Bounds are the underlying numeric type bounds combinations.
   * </td></tr>
   * </table>
   *
   * <a name="parameters"></a>
   * ## Type parameters
   * ### General parameter
   * - `name: ...` The type name. By default the type name is "numeric".
   * - `ntype: ...`: An already defined [NumericType](https://line.gitlabpages.inria.fr/aide-group/symbolingtype/NumericType.html).
   * - `left: ...` The left point field name.
   * - `right: ...` The right point field name.
   * ### Numeric type meta-data
   * - `distance: ...` The margin, i.e. the minimal distance between left and right values.
   *
   * @param {JSON|String} parameters The type parameters.
   * - By contract, the type name is a mandatory parameter, default is "value".
   *   - Note: Since each name has a different type it used to define hash, less and equal function to index type in containers, it is possible to define `std::unordered_map<symboling::Type, T>` or `std::map<symboling::Type, T>`.
   */
  class InequalityType: public Type {
    // The field names
    String name_left, name_right;
    double zero, precision;
    // The inequality margin
    double distance;
public:
    InequalityType(JSON parameters);
    InequalityType(String parameters);
    InequalityType(const char *parameters);

    virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const;
    virtual double getDistance(JSON lhs, JSON rhs) const;
    virtual wjson::Value getPath(JSON lhs, JSON rhs) const;
    virtual String asString();
  };
}
#endif
