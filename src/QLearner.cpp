#include "QLearner.hpp"
#include "RecordType.hpp"
#include "random.hpp"
#include "time.hpp"

namespace symboling {
  QLearner::QLearner(JSON parameters) : parameters(parameters), state_type(Type::getType(parameters.get("state_type", "value"))), action_type(Type::getType(parameters.get("action_type", "value")))
  {
    // Checks that a type is defined in the parameter data structure for state and action
    aidesys::alert(parameters.get("state_type", "") != state_type.getParameters().get("name", "value"), "illegal-argument", "in symboling::QLearner::QLearner trajectory parameters with an undefined state_type '%s'", parameters.asString().c_str());
    aidesys::alert(parameters.get("action_type", "") != action_type.getParameters().get("name", "value"), "illegal-argument", "in symboling::QLearner::QLearner trajectory parameters with an undefined action_type '%s'", parameters.asString().c_str());
    // Registers the Qtable symboling::Type if not yet done
    std::string qtableType = "Qtable_" + parameters.get("state_type", "") + "_" + parameters.get("action_type", "");
    if(Type::getType(qtableType).getParameters().get("name", "value") != qtableType) {
      Type::addType(new RecordType("{ name: '" + qtableType + "' types: { state: " + parameters.get("state_type", "") + " action: " + parameters.get("action_type", "") + "}}"));
    }
    qtable_type = &Type::getType(qtableType);
    qtable = new ScalarField("{ type: " + qtableType + "}");
    setParameters(parameters);
  }
  QLearner::QLearner(String parameters) : QLearner(wjson::string2json(parameters))
  {}
  QLearner::QLearner(const char *parameters) : QLearner(wjson::string2json(parameters))
  {}
  QLearner::~QLearner()
  {
    delete qtable;
  }
  QLearner& QLearner::setParameters(JSON parameters)
  {
    this->parameters.copy(parameters);
    verbose = parameters.get("verbose", verbose);
    gamma = parameters.get("gamma", gamma);
    alpha = parameters.get("alpha", alpha);
    d_max = parameters.get("d_max", d_max);
    epsilon = parameters.get("epsilon", epsilon);
    rho = epsilon > 0 ? d_max / (-log(epsilon)) : DBL_MAX;
    // -aidesys::alert(verbose, "", "(rho: %f) = (d_max: %f) / (-log(epsilon: %e))", rho, d_max, epsilon);
    K_max = parameters.get("K_max", K_max);
    bounds_count = parameters.get("bounds_count", bounds_count);
    current_result["state"] = parameters.get("initial_state", current_result["state"]);
    current_result["action"] = parameters.get("initial_action", current_result["action"]);
    current_result["time"] = parameters.get("initial_time", current_result.get("time", 0));
    max_path = parameters.get("max_path", max_path);
    max_reward = parameters.get("max_reward", max_reward);
    qtable->setParameters(parameters.clone().set("verbose", false));
    return *this;
  }
  void QLearner::doAction(wjson::Value& result) const
  {
    aidesys::alert(true, "illegal-state", "in symboling::QLearner::doAction the interaction with the environent has not be implemented");
  }
  JSON QLearner::doNext()
  {
    aidesys::alert(verbose, "", "doNext(current: '%s')", current_result.asString().c_str());
    // Here implements the generation of a new action, exploring the neighborrhood, given the current state and Qtable
    {
      JSON bounds = action_type.getBounds(bounds_count);
      // Finds either a result optimizing production or an exploration result minimizing the current result distance
      wjson::Value tmp_result = current_result, best_result = current_result, expl_result = current_result;
      double best_expected_reward = -DBL_MAX, smallest_distance = DBL_MAX;
      for(unsigned int j = 0; j < bounds.length(); j++) {
        JSON path = action_type.getPath(current_result.at("action"), bounds.at(j));
        for(unsigned int i = 1; i < path.length(); i++) {
          double d = action_type.getDistance(current_result.at("action"), path.at(i));
          if(0 < d) {
            if(d < d_max) {
              tmp_result["action"] = path.at(i);
              // Manages the best_result evaluation
              double expected_reward = qtable->getValue(tmp_result, K_max, d_max, false);
              if(expected_reward > best_expected_reward) {
                best_result["reward"] = best_expected_reward = expected_reward, best_result["action"] = path.at(i);
              }
              // Manages the expl_result evaluation
              if((d < smallest_distance || (d == smallest_distance && aidesys::random('u') < 0.5)) &&
                 !qtable->has(tmp_result))
              {
                smallest_distance = d, expl_result["reward"] = expected_reward, expl_result["action"] = path.at(i);
              }
            } else {
              break;
            }
          }
        }
      }
      aidesys::alert(verbose, "", "  best_result: %s", best_result.asString().c_str());
      aidesys::alert(verbose, "", "  expl_result: %s", expl_result.asString().c_str());
      aidesys::alert(verbose, "", "  best_expected_reward: %f current_result: %f => %s", best_expected_reward, current_result.get("reward", 0.0), best_expected_reward > current_result.get("reward", 0.0) ? "exploitation" : "exploration");
      current_result = best_expected_reward > current_result.get("reward", 0.0) ? best_result : expl_result;
    }
    // Runs one interaction with the environment
    double best_expected_reward = current_result.get("reward", 0.0);
    doAction(current_result);
    aidesys::alert(verbose, "", "doAction('%s')", current_result.asString().c_str());
    // Here implements the Qtable update with the generalized QLearing rule
    {
      if(!qtable->has(current_result)) {
        qtable->set(current_result, current_result.get("reward", 0.0));
      }
      for(auto it = qtable->edit().begin(); it != qtable->edit().end(); it++) {
        JSON state_action_it = it->first;
        double value = it->second, d = state_type.getDistance(state_action_it.at("state"), current_result.at("state"));
        if(d < d_max) {
          qtable->set(state_action_it, value + alpha * (rho == DBL_MAX ? 1 : exp(-d / rho)) * (current_result.get("reward", 0.0) + gamma * best_expected_reward - it->second));
        }
      }
      return current_result;
    }
  }
  JSON QLearner::doPath(unsigned int max_path, double max_reward)
  {
    max_path = max_path == (unsigned int) -1 ? this->max_path : max_path, max_reward = std::isnan(max_reward) ? this->max_reward : max_reward;
    static wjson::Value path;
    path.clear();
    aidesys::now(false, false);
    for(unsigned int t = 0; max_path == 0 || t < max_path; t++) {
      aidesys::alert(verbose, "", "t: %d after %.0fms of cpu ------------------------------", t, aidesys::now(false, true));
      path.add(doNext());
      if(path.at(path.length() - 1).get("reward", -DBL_MAX) >= max_reward) {
        break;
      }
    }
    return path;
  }
  class QLearnerTrajectory: public QLearner {
    Trajectory& trajectory, *p_trajectory;
public:
    QLearnerTrajectory(JSON parameters, Trajectory & trajectory, Trajectory * p_trajectory = NULL) : QLearner(parameters.clone().set("action_type", trajectory.getParameters().at("type")).set("state_type", trajectory.getParameters().at("type"))), trajectory(trajectory), p_trajectory(p_trajectory) {}
    ~QLearnerTrajectory() {
      delete p_trajectory;
    }
    void doAction(wjson::Value& result) const
    {
      result["reward"] = trajectory.getPotential(result["state"] = result["action"]);
    }
  };
  QLearner *QLearner::newQLearner(JSON parameters, Trajectory& trajectory)
  {
    return new QLearnerTrajectory(parameters, trajectory);
  }
  QLearner *QLearner::newQLearner(String parameters, Trajectory& trajectory)
  {
    return new QLearnerTrajectory(wjson::string2json (parameters), trajectory);
  }
  QLearner *QLearner::newQLearner(const char *parameters, Trajectory& trajectory)
  {
    return new QLearnerTrajectory(wjson::string2json (parameters), trajectory);
  }
  QLearner *QLearner::newQLearner(JSON parameters, JSON trajectory)
  {
    Trajectory *p_trajectory = new Trajectory(trajectory);
    return new QLearnerTrajectory(parameters, *p_trajectory, p_trajectory);
  }
  QLearner *QLearner::newQLearner(String parameters, JSON trajectory)
  {
    return newQLearner(wjson::string2json(parameters), trajectory);
  }
  QLearner *QLearner::newQLearner(const char *parameters, JSON trajectory)
  {
    return newQLearner(wjson::string2json(parameters), trajectory);
  }
  QLearner *QLearner::newQLearner(JSON parameters, String trajectory)
  {
    return newQLearner(parameters, wjson::string2json(trajectory));
  }
  QLearner *QLearner::newQLearner(String parameters, String trajectory)
  {
    return newQLearner(wjson::string2json(parameters), wjson::string2json(trajectory));
  }
  QLearner *QLearner::newQLearner(const char *parameters, String trajectory)
  {
    return newQLearner(wjson::string2json(parameters), wjson::string2json(trajectory));
  }
  QLearner *QLearner::newQLearner(JSON parameters, const char *trajectory)
  {
    return newQLearner(parameters, wjson::string2json(trajectory));
  }
  QLearner *QLearner::newQLearner(String parameters, const char *trajectory)
  {
    return newQLearner(wjson::string2json(parameters), wjson::string2json(trajectory));
  }
  QLearner *QLearner::newQLearner(const char *parameters, const char *trajectory)
  {
    return newQLearner(wjson::string2json(parameters), wjson::string2json(trajectory));
  }
  QLearner *QLearner::newQLearner(Trajectory trajectory)
  {
    return newQLearner(trajectory.getParameters(), trajectory);
  }
  QLearner *QLearner::newQLearner(JSON parameters)
  {
    return newQLearner(parameters, parameters);
  }
  QLearner *QLearner::newQLearner(String parameters)
  {
    JSON p = wjson::string2json(parameters);
    return newQLearner(p, p);
  }
  QLearner *QLearner::newQLearner(const char *parameters)
  {
    JSON p = wjson::string2json(parameters);
    return newQLearner(p, p);
  }
}
