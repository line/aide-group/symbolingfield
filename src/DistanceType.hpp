#ifndef __symboling_DistanceType__
#define __symboling_DistanceType__

#include "Value.hpp"
#include "Type.hpp"
#include "NumericType.hpp"

namespace symboling {
/**
 * @class DistanceType
 * @description Implements a type corresponding to a distance constraint.
 * <a name="description"></a>
 * - It considers points defined by a [RecordType](https://line.gitlabpages.inria.fr/aide-group/symbolingtype/RecordType.html) of the form:
 * ```
 * {
 *  left: { x: … y: … … }
 *  right: { x: … y: … … }
 * ../..
 * }
 * ```
 * where `left` and `right` are two affine points of components `x`, `y`, … corresponding to a [NumericType](https://line.gitlabpages.inria.fr/aide-group/symbolingtype/NumericType.html) of name `ntype`.
 * - This type corresponds to value for which the affine distance `d(left, right) = distance`, which borders a region such that `d(left, right) o distance`, with `o = { =, <, >}`.
 *   - Derivation is available in [this note](./distancetype.pdf).
 *
 * <a name="specification"></a>
 * ## ValueType specification
 * <table>
 * <tr style='border: 1px solid black;'><th> Syntactic projection </th><td>
 * - Returns a default value, if the underlying numeric type values are undefined.
 * - Returns a value, compatible with the requirement, if the value violates it.
 *   - For `d(left, right) = distance` do not change the value if defined.
 *   - For `d(left, right) < distance`, if  `d(left, right) >= distance`, change to `d(left, right) = distance - epsilon`.
 *   - For `d(left, right) > distance`, if  `d(left, right) <= distance`, change to `d(left, right) = distance + epsilon`.
 * where `epsilon = sqrt(DBL_EPSILON)` is an arbitrary little margin.
 * </td></tr>
 * <tr style='border: 1px solid black;'><th> Semantic projection  </th><td>
 * - Returns the closest value wich verifies the distance equality `d(left, right) = distance`.
 * </td></tr>
 * <tr style='border: 1px solid black;'><th> Distance value       </th><td>
 * - Returns the Euclidean distance between the current value.
 * </td></tr>
 * <tr style='border: 1px solid black;'><th> Geodesic path        </th><td>
 * - The geodesic is calculated using the underlying numeric type precision increment.
 * </td></tr>
 * <tr style='border: 1px solid black;'><th> Comparison           </th><td>
 * - Returns <tt>NAN</tt> no comparison possible for this type.
 * </td></tr>
 * <tr style='border: 1px solid black;'><th> Bounds           </th><td>
 * - Bounds are the underlying numeric type bounds combinations.
 * </td></tr>
 * </table>
 *
 * <a name="parameters"></a>
 * ## Type parameters
 * ### General parameter
 * - `name: ...` The type name. By default the type name is "numeric".
 * - `left: ...` The left point field name.
 * - `right: ...` The right point field name.
 * - `components: [x y z ...]` A list of the component names.
 * - `ntype: ...`: An already defined [NumericType](https://line.gitlabpages.inria.fr/aide-group/symbolingtype/NumericType.html).
 * ### Numeric type meta-data
 * - `distance: ...`: The distance between left and right 2dpoints.
 * - `sign: …` : Either '>', '<', '=' (default), this depending on the [in]equality `d(left, right) o distance` of the underlying regionxf.
 *
 * @param {JSON|String} parameters The type parameters.
 */
  class DistanceType: public Type {
#ifndef SWIG
    // Defines the value to double interface
    class Point {
public:
      wjson::Value components;
      const NumericType& ntype;
      unsigned int dimension;
      double zero;
      double *values;
      // Constructs a point for a given value, considering the distance region parameters
      Point(JSON parameters, JSON value);
      // Constructs a point from `e1 point1 + e2 point2`
      Point(JSON parameters, double e1, const Point& point1, double e2, const Point& point2);
      // Constructs a point from `epoint1 + e2 point2`
      Point(JSON parameters, const Point& point1, double e2, const Point& point2);
      // Constructs a point from `e point1 + (1 - e) point2`
      Point(JSON parameters, double e, const Point& point1, const Point& point2);
      Point(JSON parameters);
      ~Point();
      // Reports this point in the value
      void report(wjson::Value& value) const;
      // Returns the affine distance with respect to another point
      double getDistance(const Point& point) const;
    };
#endif
    //
    // The required distance
    double distance;
    // The inequality margin
    double margin;
    // Field names
    String name_left, name_right;
    // Numerical type
    double zero, precision;
public:
    DistanceType(JSON parameters);
    DistanceType(String parameters);
    DistanceType(const char *parameters);
    virtual wjson::Value getValue(JSON value, bool semantically_else_syntactically = true) const;
    virtual double getDistance(JSON lhs, JSON rhs) const;
    virtual wjson::Value getPath(JSON lhs, JSON rhs) const;
    virtual String asString();
  };
}
#endif
