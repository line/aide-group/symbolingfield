#ifndef __symboling_Trajectory__
#define __symboling_Trajectory__

#include "Value.hpp"
#include "Type.hpp"
#include "ScalarField.hpp"
#include <unordered_map>
#include <unordered_set>

namespace symboling {
  /**
   * @class Trajectory
   * @description Implements a trajectory generator on a symboling space.
   *
   * <a name="parameters"></a>
   * ## Trajectory parameters
   * ### General parameter
   * - `type: ...` The type element used to calculate the distance, mandatory, default is "value".
   * - `epsilon: ...` The minimal distance between to indistinguishable value, default is near, i.e., `1e-12`.
   *   - If `d(current, goal) < epsilon` the goal is supposed to be reached.
   *   - If `d(current, obstacle) < epsilon` the obstacle is supposed to be hit, generating an error.
   * - `step: ...` The neighborhood minimal weighted distance to an obstacle, default is 1.
   * - `d_max: ...` The maximal distance for extrapolation away from obstacle, default is no bound.
   * - `nu_max: ...` The maximal deviation from a geodesic regarding extrapolation, default is no bound.
   * - `bounds_count: ...` The maximal number of bounds to take into account for extrapolation, default is 0 (i.e., all bounds).
   * - `w_max: ...` The maximal repulsion weight to avoid an obstacle, default is 1/near, i.e., `1e+12`.
   * - `points`: Optionally defines trajectory weighted (corresponding to the output) points:
   * ```
   * points : [
   *   { input: ... output: ... }
   *   ../..
   * ]
   * ```
   * - `regions`: Optionally defines trajectory weighted (corresponding to the output) regions, i.e., obstacles, defined by types:
   * ```
   * regions : [
   *   { name: ... type: ... output: ... type_parameter: ... }
   *   ../..
   * ]
   * ```
   * - `verbose: ...` If true print the trajectory steps on stdout. Default is false.
   *
   * @param {JSON|String} parameters The trajectory parameters.
   */
  class Trajectory {
    // Trajectory parameters
    wjson::Value parameters;
    const Type& type;
    double epsilon = 1e-12, step = 1, d_max = DBL_MAX, w_max = 1e+12;
    bool verbose = false;
    std::unordered_map < Type *, double > stateRegions;
    // Trajectory current state
    ScalarField statePoints;
    std::unordered_set < wjson::Value > visitedPoints;
    wjson::Value current = wjson::Value::EMPTY;
    bool done = false;
    void clear();

    // Potential evaluation
public:
    Trajectory(JSON parameters);
    Trajectory(String parameters);
    Trajectory(const char *parameters);
    virtual ~Trajectory() {}

    /**
     * @function getParameters
     * @memberof Trajectory
     * @instance
     * @description Returns the trajectory parameters.
     * @return {JSON} The trajectory parameters.
     */
    JSON getParameters() const
    {
      return parameters;
    }
    /**
     * @function setParameters
     * @memberof Trajectory
     * @instance
     * @description Modifies some Trajectory parameters.
     * - All parameters but the `type` can be modified.
     * - The Trajectory points are preserved when changing the parameters, thus using the `points` field simply adds the points.
     * @param {JSON} parameters The Trajectory parameters.
     * @return {Trajectory} This value allowing to chain methods.
     */
    Trajectory& setParameters(JSON parameters);

    /**
     * @function addPoint
     * @memberof Trajectory
     * @instance
     * @description Adds/Erases a punctual goal or a constraint for this trajectory.
     * @param {Value} value A goal or constraint to add, as punctual value.
     * @param {double} weight The element weight or relative reward.
     * - `> 0` If the point belongs to a goal. The highest the value, the more the goal importance.
     * - `< 0` If the point belongs to a constraint or obstacle to avoid.
     * - `= 0` If the point is to be deleted.
     * @return {Trajectory} This value allowing to chain add methods.
     */
    Trajectory& addPoint(JSON value, double weight);

    /**
     * @function addRegion
     * @memberof Trajectory
     * @instance
     * @description Adds/Erases a regaion goal or a constraint for this trajectory.
     * @param {Type} value A goal or constraint to add, as a region defined by a type.
     * - By contract a Type is remament, i.e., it is NOT deleted during the program execution.
     * @param {double} weight The element weight or relative reward.
     * - `> 0` If the point belongs to a goal. The highest the value, the more the goal importance.
     * - `< 0` If the point belongs to a constraint or obstacle to avoid.
     * - `= 0` If the point is to be deleted.
     * @return {Trajectory} This value allowing to chain add methods.
     */
    Trajectory& addRegion(Type region, double weight);

    /**
     * @function add
     * @memberof Trajectory
     * @instance
     * @description Adds all values trajectory values.
     * @param {JSON|String} values The trajectory points or regions values.
     * - The trajectory points's type must be compatible (i.e., the same type or a derived type) of this scalar value type.
     * - It can also be defined as a JSON unordered list of value of syntax:
     * ```
     * [
     *   { input: ... output: ... } // for a point
     *   { type: ... output: ... type_parameter: ... } // for a region
     *   ../..
     * ]
     * ```
     * where `input` stands for the trajectory point, `type` for the region predefined type (adding type parameters if required), and `output` for the related weight.
     * @return {Trajectory} This value allowing to chain add methods.
     */
    Trajectory& add(JSON values);
    Trajectory& add(String values);
    Trajectory& add(const char *values);

    /** Returns the trajectory defined values, for debugging purpose.
     * @return {JSON} The current trajectory scalar field values.
     */
    JSON getValues() const
    {
      return statePoints.getValues();
    }
    /** Returns the subharmonic potential estimation at a current location.
     * @param {JSON|String} value The potential input.
     * @return {Value} The current trajectory scalar field values.
     */
    double getPotential(JSON value);
    double getPotential(String value);
    double getPotential(const char *value);

    /**
     * @function getNext
     * @memberof Trajectory
     * @instance
     * @description Gets the next point on the trajectory.
     * @param {JSON|String} [current] The actual state value.
     * - If omitted considers the previous returned value.
     * - If specified then reset the trajectory.
     * @return {Value} The next value on the trajectory towards a goal, avoiding obstacles. Returns the current value if already on a goal.
     * @throws An `illegal-argument` exception if at the beginning the value was already on a obstacle.
     */
    JSON getNext(JSON current);
    JSON getNext(String current);
    JSON getNext(const char *values);
    JSON getNext();

    /**
     * @function isDone
     * @memberof Trajectory
     * @instance
     * @description Checks if a goal has been attained.
     * @return {bool} True if the last current value obtained by [`getNext()`](#.getNext) is on a goal.
     */
    bool isDone() const
    {
      return done;
    }
    /**
     * @function getPath
     * @memberof Trajectory
     * @instance
     * @description Gets the path from the current state to a goal.
     * - Parameters can also be specified via the ScalarField "current", "max_path" parameters.
     * @param {JSON|String} [current] The actual state value.
     * - If omitted considers the previous returned value.
     * - If specified then reset the trajectory.
     * @param {unit} [max_path=0] The maximal path length, default is 0 (no bound);
     * @return {Value} The path sequence of the trajectory towards a goal, avoiding obstacles.
     * @throws An `illegal-argument` exception if at the beginning the value was already on a obstacle.
     */
    JSON getPath(JSON current, unsigned int max_path = -1);
    JSON getPath(String current, unsigned int max_path = -1);
    JSON getPath(const char *current, unsigned int max_path = -1);
    JSON getPath(unsigned int max_path = -1);
private:
    unsigned int max_path = 0;
  };
}
#endif
