#include "DistanceType.hpp"
#include <cfloat>

namespace symboling {
  DistanceType::Point::Point(JSON parameters) : components(parameters.get("components")), ntype((const NumericType&) getType(parameters.get("ntype"))), dimension(components.length()), zero(ntype.getParameters().get("zero", 0.0)), values(new double[dimension])
  {}
  DistanceType::Point::Point(JSON parameters, JSON value) : Point(parameters)
  {
    for(unsigned int i = 0; i < dimension; i++) {
      values[i] = value.get(components.get(i, ""), zero);
    }
  }
  DistanceType::Point::Point(JSON parameters, double e1, const Point& point1, double e2, const Point& point2) : Point(parameters)
  {
    for(unsigned int i = 0; i < dimension; i++) {
      values[i] = e1 * point1.values[i] + e2 * point2.values[i];
    }
  }
  DistanceType::Point::Point(JSON parameters, const Point& point1, double e2, const Point& point2) : Point(parameters)
  {
    for(unsigned int i = 0; i < dimension; i++) {
      values[i] = point1.values[i] + e2 * point2.values[i];
    }
  }
  DistanceType::Point::Point(JSON parameters, double e, const Point& point1, const Point& point2) : Point(parameters)
  {
    for(unsigned int i = 0; i < dimension; i++) {
      values[i] = point2.values[i] + e * (point1.values[i] - point2.values[i]);
    }
  }
  DistanceType::Point::~Point()
  {
    delete[] values;
  }
  void DistanceType::Point::report(wjson::Value& value) const
  {
    for(unsigned int i = 0; i < dimension; i++) {
      value.set(components.get(i, ""), values[i]);
    }
  }
  double DistanceType::Point::getDistance(const Point& point) const
  {
    double d2 = 0;
    for(unsigned int i = 0; i < dimension; i++) {
      double d = values[i] - point.values[i];
      d2 += d * d;
    }
    return sqrt(d2);
  }
  static bool DistanceTypeRegistered = Type::addType("DistanceType", [] (JSON parameters) { return new DistanceType(parameters);
                                                     });
  DistanceType::DistanceType(JSON parameters) : Type("{name: DistanceType}"), distance(parameters.get("distance", 0.0)), name_left(parameters.get("left", "")), name_right(parameters.get("right", ""))
  {
    // Checks the parameters
    aidesys::alert(!parameters.isMember(name_left), "illegal-argument", "in symbolingfield::DistanceType::DistanceType 'left' paremeter undefined");
    aidesys::alert(!parameters.isMember(name_right), "illegal-argument", "in symbolingfield::DistanceType::DistanceType 'right' paremeter undefined");
    aidesys::alert(!(parameters.isMember("components") && parameters.at("components").isArray()), "illegal-argument", "in symbolingfield::DistanceType::DistanceType 'components' parameter undefined or ill-defined");
    aidesys::alert("NumericType" != getType(parameters.get("ntype", "")).getParameters().get("name", ""), "illegal-argument", "in symbolingfield::DistanceType::DistanceType 'ntype' parameter undefined or ill-defined");
    const NumericType& ntype = (const NumericType&) getType(parameters.get("ntype", ""));
    precision = ntype.getParameters().get("precision", NAN);
    char s = parameters.get("sign", "=")[0];
    margin = s == '<' ? -sqrt(DBL_EPSILON) : s == '>' ? sqrt(DBL_EPSILON) : 0;
    aidesys::alert(std::isnan(precision), "illegal-argument", "in symbolingfield::DistanceType::DistanceType 'ntype' precision parameter undefined or ill-defined");
    // Sets the bounds
    {
      JSON components = parameters.at("components");
      unsigned int dimension = components.length();
      for(unsigned int b = 0; b < (bounds_count = (int) pow(2, dimension + 1)); b++) {
        wjson::Value bound;
        for(unsigned int i = 0, j = 0; i < dimension; i++, j += 2) {
          bound[name_left].set(components.get(i, ""), ntype.getParameters().get((b >> j) % 2 ? "min" : "max", NAN));
          bound[name_right].set(components.get(i, ""), ntype.getParameters().get((b >> (j + 1)) % 2 ? "min" : "max", NAN));
        }
        bounds.add(bound);
      }
    }
  }
  DistanceType::DistanceType(String parameters) : DistanceType(wjson::string2json(parameters))
  {}
  DistanceType::DistanceType(const char *parameters) : DistanceType(wjson::string2json(parameters))
  {}
  wjson::Value DistanceType::getValue(JSON value, bool semantically_else_syntactically) const
  {
    wjson::Value result = value;
    Point left_(getParameters(), value.at(name_left)), right_(getParameters(), value.at(name_right));
    if(semantically_else_syntactically || margin != 0) {
      Point middle(getParameters(), 0.5, left_, right_), sub(getParameters(), left_, -1, right_);
      double d_ = left_.getDistance(right_), d2_ = d_ == 0 ? margin : (distance + margin) / (2 * d_);
      Point left(getParameters(), middle, d2_, sub), right(getParameters(), middle, -d2_, sub);
      left.report(result[name_left]);
      right.report(result[name_right]);
    } else {
      left_.report(result[name_left]);
      right_.report(result[name_right]);
    }
    return result;
  }
  double DistanceType::getDistance(JSON lhs, JSON rhs) const
  {
    if(Type::justDone(lhs, rhs)) {
      return Type::getDistance(lhs, rhs);
    }
    Point ll(getParameters(), lhs.at(name_left)), lr(getParameters(), lhs.at(name_right)), rl(getParameters(), lhs.at(name_left)), rr(getParameters(), rhs.at(name_right));
    double dl = ll.getDistance(rl), dr = lr.getDistance(rr);
    return sqrt(dl * dl + dr * dr);
  }
  wjson::Value DistanceType::getPath(JSON lhs, JSON rhs) const
  {
    if(!Type::justDone(lhs, rhs)) {
      DistanceType::getDistance(lhs, rhs);
    }
    Point ll(getParameters(), lhs.at(name_left)), lr(getParameters(), lhs.at(name_right)), rl(getParameters(), lhs.at(name_left)), rr(getParameters(), rhs.at(name_right));
    wjson::Value path, v = lhs;
    for(double e = 0; e <= 1; e = std::min(1.0, e + precision)) {
      Point vl(getParameters(), e, rl, ll), vr(getParameters(), e, lr, rr);
      vl.report(v[name_left]), vr.report(v[name_right]);
      path.add(v);
    }
    return path;
  }
  String DistanceType::asString()
  {
    static std::string string;
    string = Type::asString();
    string = string.substr(0, string.length() - 1) + " parameters: " + getParameters().asString() + "}";
    return string;
  }
}
