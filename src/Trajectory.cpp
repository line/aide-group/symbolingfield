#include "Trajectory.hpp"
#include <math.h>
#include <float.h>
#include "time.hpp"

namespace symboling {
  Trajectory::Trajectory(JSON parameters) : parameters(parameters), type(Type::getType(parameters.get("type", "value"))), statePoints(parameters.clone().set("verbose", false))
  {
    setParameters(parameters);
  }
  Trajectory::Trajectory(String parameters) : Trajectory(wjson::string2json(parameters))
  {}
  Trajectory::Trajectory(const char *parameters) : Trajectory(wjson::string2json(parameters))
  {}
  Trajectory& Trajectory::setParameters(JSON parameters)
  {
    this->parameters.copy(parameters);
    verbose = parameters.get("verbose", verbose);
    epsilon = parameters.get("epsilon", epsilon);
    step = parameters.get("step", step);
    d_max = parameters.get("d_max", d_max);
    w_max = parameters.get("w_max", w_max);
    current = parameters.get("current", current);
    max_path = parameters.get("max_path", max_path);
    statePoints.setParameters(parameters);
    if(parameters.isMember("regions")) {
      add(parameters.at("regions"));
    }
    return *this;
  }
  void Trajectory::clear()
  {
    done = false;
    visitedPoints.clear();
  }
  Trajectory& Trajectory::addPoint(JSON value, double weight)
  {
    clear();
    if(weight == 0) {
      statePoints.erase(value);
    }else {
      statePoints.set(value, weight);
    }
    return *this;
  }
  Trajectory& Trajectory::addRegion(Type region, double weight)
  {
    clear();
    if(weight == 0) {
      stateRegions.erase(&region);
    }else {
      stateRegions.insert_or_assign(&region, weight);
    }
    return *this;
  }
  Trajectory& Trajectory::add(JSON values)
  {
    for(unsigned int l = 0; l < values.length(); l++) {
      JSON value = values.at(l);
      if(value.isMember("input")) {
        addPoint(value.at("input"), value.get("output", NAN));
      }
      if(value.isMember("type") && value.isMember("name")) {
        Type::addType(value);
        addRegion(Type::getType(value.get("name", "")), value.get("output", NAN));
      }
    }
    return *this;
  }
  Trajectory& Trajectory::add(String values)
  {
    return add(wjson::string2json(values));
  }
  Trajectory& Trajectory::add(const char *values)
  {
    return add(wjson::string2json(values));
  }
  double Trajectory::getPotential(JSON value)
  {
    double result = 0;
    for(auto it = statePoints.get().cbegin(); it != statePoints.get().cend(); it++) {
      if(it->second != 0) {
        double d = statePoints.getDistance(current, it->first);
        if(d < epsilon) {
          return it->second > 0 ? DBL_MAX : -DBL_MAX;
        }
        result += it->second / d;
      }
    }
    statePoints.set(value, result);
    return result;
  }
  double Trajectory::getPotential(String value)
  {
    return getPotential(wjson::string2json(value));
  }
  double Trajectory::getPotential(const char *value)
  {
    return getPotential(wjson::string2json(value));
  }
  JSON Trajectory::getNext(JSON value)
  {
    current = value;
    clear();
    return getNext();
  }
  JSON Trajectory::getNext(String value)
  {
    return getNext(wjson::string2json(value));
  }
  JSON Trajectory::getNext(const char *value)
  {
    return getNext(wjson::string2json(value));
  }
  JSON Trajectory::getNext()
  {
    aidesys::alert(verbose, "", "> getNext(current='%s')", current.asString().c_str());
    // Iterates on regions to add projections
    for(auto it = stateRegions.begin(); it != stateRegions.end(); it++) {
      if(it->second != 0) {
        addPoint(it->first->getValue(current), it->second);
      }
    }
    // Finds the weighted closest obstacle and goal points
    double dw_min_p = DBL_MAX, d_min_n = DBL_MAX, dw_min_n = DBL_MAX;
    const wjson::Value *v_min_p, *v_min_n;
    double w_min_p, w_min_n;
    for(auto it = statePoints.get().cbegin(); it != statePoints.get().cend(); it++) {
      if(it->second != 0) {
        double d = statePoints.getDistance(current, it->first), dw = d / fabs(it->second);
        if(it->second > 0) {
          if(d < epsilon) {
            done = true;
            return current = it->first;
          }
          if(dw < dw_min_p) {
            dw_min_p = dw;
            v_min_p = &it->first;
            w_min_p = it->second;
          }
        } else {
          aidesys::alert(d < epsilon, "illegal-argument", "in symboling::Trajectory::getNext, the current value '" + current.asString() + "' is too close to the obstacle '" + it->first.asString() + "', either you started to close to an obstacle or your potential field is ill-defined with respect to the algorithm");
          if(dw < dw_min_n) {
            dw_min_n = dw;
            v_min_n = &it->first;
            w_min_n = it->second;
          }
          if(d < d_min_n) {
            d_min_n = d;
          }
        }
      }
    }
    aidesys::alert(verbose, "", "\t> d/w(current, goal='%s') = %f d/w(current, obstacle='%s') = %f", v_min_p->asString().c_str(), dw_min_p, v_min_n->asString().c_str(), dw_min_n);
    if(dw_min_p < dw_min_n || step < d_min_n) {
      // Implements the close goal strategy
      {
        aidesys::alert(verbose, "", "\t> Goal directing");
        JSON path = type.getPath(current, *v_min_p);
        current = path.length() > 1 ? path.at(1) : *v_min_p;
        if(statePoints.getDistance(current, *v_min_p) < epsilon) {
          done = true;
          return current;
        }
        if(statePoints.getDistance(current, *v_min_n) > step) {
          return current;
        } else {
          aidesys::alert(verbose, "", "\t\t but d(current='%s', obstacle): %f <= step: %f", current.asString().c_str(), statePoints.getDistance(current, *v_min_n), step);
        }
      }
    }
    // Implements the obstacle avoidance strategy
    {
      aidesys::alert(verbose, "", "\t> Obstacle avoidance");
      wjson::Value previous = current;
      // Increments the weights to avoid local maxima and loops
      w_min_n = statePoints.edit()[*v_min_n] *= 2;
      // Local value of the potential
      double v_0 = w_min_p / statePoints.getDistance(current, *v_min_p) + w_min_n / statePoints.getDistance(current, *v_min_n);
      // Search in an increasing neigborhood
      for(double d = d_max; d < DBL_MAX / 2; d *= sqrt(2)) {
        wjson::Value nexts = statePoints.getExtrapolations(current, *v_min_n, d);
        // Maximizes the local value of the augmented potential V({Obst,Goal}) + 1 / d(current, next)
        double va_next = -DBL_MAX;
        for(unsigned int i = 0; i < nexts.length(); i++) {
          double d_0 = statePoints.getDistance(nexts.at(i), previous), d_p = statePoints.getDistance(nexts.at(i), *v_min_p), d_n = statePoints.getDistance(nexts.at(i), *v_min_n);
          if(d_0 > 0 && d_p > 0 && d_n > 0) {
            double v_i = w_min_p / d_p + w_min_n / d_n, va_i = v_i + 1 / d_0;
            // - aidesys::alert(verbose, "", "\t>> for Next(%d) : '%s' {d_0 : %f d_p: %f d_n: %f} (v_i: %f > v_0: %f && va_i: %f > va_next: %f && new: %d) in B(current, %f >= %f)", i, nexts.at(i).asString().c_str(), d_0, d_p, d_n, v_i, v_0, va_i, va_next == -DBL_MAX ? -1111 : va_next, visitedPoints.find(nexts.at(i)) == visitedPoints.end(), d, d_max);
            // Here requires the potential to increase and choose the new point with best augmented potential
            if(v_i > v_0 && va_i > va_next && visitedPoints.find(nexts.at(i)) == visitedPoints.end()) {
              va_next = va_i, current = nexts.at(i);
            }
          } else {
            aidesys::alert(verbose, " illegal-state", "in symboling::Trajectory::getNext() d_0 = %f, d_p = %f, d_n = %f zero distance should not happen", d_0, d_p, d_n);
          }
        }
        if(va_next > -DBL_MAX) {
          aidesys::alert(verbose, "", ">> Next : '%s' V = %f / (d(next, goal) = %f) + %f / (d(next, obstacle) = %f) d(next, current) = %f nu_next = %f among %d extrapolations in B('%s', %f >= %f)", current.asString().c_str(), w_min_p, statePoints.getDistance(current, *v_min_p), w_min_n, statePoints.getDistance(current, *v_min_n), statePoints.getDistance(current, previous), statePoints.getDistance(current, previous) + statePoints.getDistance(current, *v_min_n) - statePoints.getDistance(current, *v_min_n), nexts.length(), previous.asString().c_str(), d, d_max);
          visitedPoints.insert(current);
          return current;
        }
      }
      if(verbose) {
        for(auto it = visitedPoints.begin(); it != visitedPoints.end(); it++) {
          printf("visitedPoints += '%s'\n", it->asString().c_str());
        }
      }
      aidesys::alert(true, "illegal-state", "in symboling::Trajectory::getNext() stucked in a local minimum, no extrapolation possibility: may be \n - nu_max or bounds_count is too small or \n -repulsion has thrown outside the nounded space : '%s' visited point: #%d ", statePoints.getParameters().asString().c_str(), visitedPoints.size());
      return current;
    }
  }
  JSON Trajectory::getPath(JSON value, unsigned int max_path)
  {
    current = value;
    clear();
    return getPath(max_path);
  }
  JSON Trajectory::getPath(String value, unsigned int max_path)
  {
    return getPath(wjson::string2json(value), max_path);
  }
  JSON Trajectory::getPath(const char *value, unsigned int max_path)
  {
    return getPath(wjson::string2json(value), max_path);
  }
  JSON Trajectory::getPath(unsigned int max_path)
  {
    max_path = max_path == (unsigned int) -1 ? this->max_path : max_path;
    static wjson::Value path;
    path.clear();
    path[0] = current;
    aidesys::now(false, false);
    for(unsigned int t = 1; (t <= max_path || max_path == 0) && !isDone(); t++) {
      aidesys::alert(verbose, "", "t: %d after %.0fms of cpu ------------------------------", t, aidesys::now(false, false));
      path[t] = getNext();
    }
    return path;
  }
}
