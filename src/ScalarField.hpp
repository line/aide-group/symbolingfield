#ifndef __symboling_ScalarField__
#define __symboling_ScalarField__

#include "Value.hpp"
#include "Type.hpp"
#include <unordered_map>
#include <float.h>

namespace symboling {
  /**
   * @class ScalarField
   * @description Implements a real-value function on a symboling space.
   *
   * <a name="parameters"></a>
   * ## ScalarField parameters
   * ### General parameter
   * - `type: ...` The type element used to calculate the distance, mandatory, default is "value".
   * - `epsilon: ...` The threshold between to indistinguishable numerical value, default is `1e-12`.
   * - `points`: Optionally defines weighted points:
   * ```
   * points : [
   *   { input: ... output: ... }
   *   ../..
   * ]
   * ```
   * - `verbose: ...` If true print the interpolation, extrapolation and barycenter intermediate values. Default is false.
   * @param {JSON|String} parameters The scalar-field parameters.
   */
  class ScalarField {
    wjson::Value parameters;
    const Type& type;
    double epsilon = 1e-12;
    bool verbose = false;
    std::unordered_map < wjson::Value, double > values;
    // This mask allows to cache barycenter and diameter calculations
    mutable unsigned int modified = 0;
public:
    ScalarField(JSON parameters);
    ScalarField(String parameters);
    ScalarField(const char *parameters);
    virtual ~ScalarField() {}

    /**
     * @function getParameters
     * @memberof ScalarField
     * @instance
     * @description Returns the trajectory parameters.
     * @return {JSON} The trajectory parameters.
     */
    JSON getParameters() const
    {
      return parameters;
    }
    /**
     * @function setParameters
     * @memberof ScalarField
     * @instance
     * @description Modifies some ScalarField parameters.
     * - All parameters but the `type` can be modified.
     * - The ScalarField points are preserved when changing the parameters, thus using the `points` field simply adds the points.
     * @param {JSON} parameters The Trajectory parameters.
     * @return {ScalarField} This value allowing to chain methods.
     */
    ScalarField& setParameters(JSON parameters);

    /**
     * @function set
     * @memberof ScalarField
     * @instance
     * @description Adds/Replaces a punctual value for this scalar-field.
     * @param {Value} input The input data structure
     * @param {double} [output=NAN] The corresponding output value.
     * - If equals NAN the value is undefined, and only used for extrapolation.
     * @return {ScalarField} This value allowing to chain add methods.
     */
    ScalarField& set(JSON input, double output = NAN);

    /**
     * @function erase
     * @memberof ScalarField
     * @instance
     * @description Deletes a punctual value for this scalar-field.
     * @param {Value} input The input data structure
     * @return {ScalarField} This value allowing to chain add methods.
     */
    ScalarField& erase(JSON input);

    /**
     * @function clear
     * @memberof ScalarField
     * @instance
     * @description Deletes all values of this scalar-field.
     * @param {Value} input The input data structure
     * @return {ScalarField} This value allowing to chain add methods.
     */
    ScalarField& clear();

    /**
     * @function add
     * @memberof ScalarField
     * @instance
     * @description Adds all values of a scalar-field to this one.
     * @param {ScalarField|JSON|String} field The scalar-field to copy or the scalar-field value or values.
     * - The scalar-field value's type must be compatible (i.e., the same type or a derived type) of this scalar value type.
     * - It can also be defined as a JSON unordered list of point with output value, using the syntax:
     * ```
     * [
     *   { input: ... output: ... }
     *   ../..
     * ]
     * ```
     * - It can also be defined as a JSON unordered list of input point with undefined value, using the syntax:
     * ```
     * [
     *   input_1
     *   ../..
     * ]
     * ```
     * - It can also be defined as a point with output value
     * ```
     * { input: ... output: ... }
     * ```
     * - It can also be defined as a unique point with undefined value.
     * @return {ScalarField} This value allowing to chain add methods.
     */
    ScalarField& add(const ScalarField& field);
    ScalarField& add(JSON values);
    ScalarField& add(String values);
    ScalarField& add(const char *values);

    /**
     * @function load
     * @memberof ScalarField
     * @instance
     * @description Loads all values of a scalar-field strored in a wJSON or JSON file.
     * - It is short-cut for `add(wjson::string2json(aidesys::load(filename)))`.
     * @param {String} filename The file name.
     * @return {ScalarField} This value allowing to chain add methods.
     */
    ScalarField& load(String filename);

    /**
     * @function save
     * @memberof ScalarField
     * @instance
     * @description Saves all values of a scalar-field strored in a wjson file.
     * - It is short-cut for `aidesys::save(filename, getValues().asString(pretty, strict))`.
     * @param {String} filename The file name.
     * @param {bool} [pretty=false] If true properly format in 2D, else returns a minimal raw format.
     * @param {bool} [strict=false] If true [strict JSON](https://www.json.org/json-en.html) syntax, else produces a [weak JSON](https://line.gitlabpages.inria.fr/aide-group/wjson/index.html) syntax.
     */
    void save(String filename, bool pretty = false, bool strict = false) const;

    /**
     * @function getValues
     * @memberof ScalarField
     * @instance
     * @description Returns the scalar-field defined values.
     * @return {JSON} The current scalar-field values:
     * ```
     * [
     *   { input: ... output: ... }
     *   ../..
     * ]
     * ```
     */
    JSON getValues() const;

    /**
     * @function has
     * @memberof ScalarField
     * @instance
     * @description Returns true if the point is stored in the field table.
     * @param {JSON} input The data point to check.
     * @param {bool} [NaN=false] If true includes also points with not-a-number output value.
     * @return {bool} True if the point is explicitly stored.
     */
    bool has(JSON input, bool NaN = false)
    {
      auto it = values.find(input);
      return it != values.end() && (NaN || !std::isnan(it->second));
    }
    /**
     * @function get
     * @memberof ScalarField
     * @instance
     * @description Defines an iterator over the scalarfield values, used in a construct of the form:
     * ```
     *  for(auto it = scalarfield.get().cbegin(); it != scalarfield.get().cend(); it++) {
     *    JSON input& = it->first;
     *    double output = it->second;
     *    ../..
     *  }
     * ```
     * - The `scalarfield.get().size()` construct returns the number of predefined values in this field.
     * - The `scalarfield.edit()` construct returns a modifiable `std::unordered_map < wjson::Value, double >` allowing to edit the predefined values.
     * @return A `const std::unordered_map < wjson::Value, double >&` reference for iteration.
     */
    const std::unordered_map < wjson::Value, double >& get() const {
      return values;
    }
    std::unordered_map < wjson::Value, double > &edit() {
      return values;
    }

    /**
     * @function getValue
     * @memberof ScalarField
     * @instance
     * @description Returns a punctual interpolated value for this scalar-field.
     * - Parameters can also be specified via the ScalarField "K_max", "d_max", "save_interpolation" parameters.
     * @param {Value} input The input data structure
     * @param {uint} [K_max=-1] The maximal cardinal of the interpolation neighborhood:
     * - `0`: Returns the exact value if set, and NAN otherwise.
     * - `1`: Returns the closest exact value.
     * - `2`: Returns the closest value on the path between the two closest value.
     * - `> 2`: Reduces iteratively the neighborhood size by interpolating the value
     *   - between the closest values in the neighborhood,
     *   - choosing the value which as the closest to the input.
     * - `-1`: Unbounded neighborhood cardinal, only the maximal distance is considered.
     * @param {double} [d_max=DBL_MAX] The maximal neighborhood distance, unbounded if equals to NAN.
     * @param {bool} [save_interpolation=false] If true save the interpolatd value for further use.
     * @return {double} The exact or interpolated value, 0 by default.
     */
    double getValue(JSON input, unsigned int K_max = -1, double d_max = NAN, bool save_interpolation = false);
private:
    unsigned int K_max = -1;
    double d_max = DBL_MAX;
    bool save_interpolation = false;
public:

    /**
     * @function getExtrapolations
     * @memberof ScalarField
     * @instance
     * @description Returns extrapolated values for this scalar-field.
     * - Given type bounds to input related geodesics, the points below `d_max`, on the bound geodesics are computed, as soon as their distance to the reference is higher that for the input.
     * - Parameters can also be specified via the ScalarField "d_max", "nu_max", "bound_counts" parameters.
     * @param {Value} input The input data structure.
     * @param {Value} reference The reference data structure.
     * @param {double} [d_max=DBL_MAX] The maximal neighborhood distance, unbounded if equals to DBL_MAX or NAN.
     * @param {double} [nu_max=DBL_MAX] The maximal deviation `nu = d(value, input) + d(input, reference) - d(value, reference)` from the geodesic equality, unbounded if equals to DBL_MAX or NAN.
     * @param {int} [bounds_count = 0] The type bounds count to be taken into account. By default all bounds are considered.
     * @return {Value} A list of extrapolated data structures value approximating the prolongation of the input with respect to the reference.
     */
    wjson::Value getExtrapolations(JSON input, JSON reference, double d_max = NAN, double nu_max = NAN, unsigned int bounds_count = -1);
private:
    double nu_max = DBL_MAX;
    unsigned int bounds_count = 0;
public:

    /**
     * @function getBarycenter
     * @memberof ScalarField
     * @instance
     * @description Returns the weighted centroid value of the scalar-field.
     * - Iteratively
     *   - detect the closest values and consider the path between them
     *   - choosing in their path the value which distance balance corresponds as much as possible to the two values relative weight.
     * - This is illustrated on this [tiny demo page](./barycenterdemo/type-field-barycenter.html).
     * @return {value} A data structure value which distance to the other values is weighted by the scalar-field values.
     */
    wjson::Value getBarycenter() const;
private:
    mutable wjson::Value barycenter;
public:

    /**
     * @function getDiameter
     * @memberof ScalarField
     * @instance
     * @description Returns the maximal distance between all predefined values.
     * @return {double} The computed diameter.
     */
    double getDiameter() const;
private:
    mutable double diameter = -1;
public:

    /** @function getDistance
     * @memberof ScalarField
     * @instance
     * @description Returns the distance between two data value, given the ScalarField type.
     * - Values are cached avoiding to be recalculated twice.
     * @param {Value} lhs The left hand data structure.
     * @param {Value} rhs The left hand data structure.
     * @return {double} The distance value.
     */
    double getDistance(JSON lhs, JSON rhs) const;
private:
    mutable std::unordered_map < wjson::Value, std::unordered_map < wjson::Value, double >> distances;
  };
}

#endif
