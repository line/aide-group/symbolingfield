#include "InequalityType.hpp"

namespace symboling {
  static bool InequalityTypeRegistered = Type::addType("InequalityType", [] (JSON parameters) { return new InequalityType(parameters);
                                                       });
  InequalityType::InequalityType(JSON parameters) : Type("{name: InequalityType}"), name_left(parameters.get("left", "")), name_right(parameters.get("right", ""))
  {
    // Checks the parameters
    aidesys::alert(!parameters.isMember("left"), "illegal-argument", "in symbolingfield::InequalityType::InequalityType 'left' paremeter undefined");
    aidesys::alert(!parameters.isMember("right"), "illegal-argument", "in symbolingfield::InequalityType::InequalityType 'right' paremeter undefined");
    aidesys::alert("NumericType" != getType(parameters.get("ntype", "")).getParameters().get("name", ""), "illegal-argument", "in symbolingfield::InequalityType::InequalityType 'ntype' parameter undefined or ill-defined");
    const NumericType& ntype = (const NumericType&) getType(parameters.get("ntype", ""));
    zero = ntype.getParameters().get("zero", 0.0), precision = ntype.getParameters().get("precision", NAN);
    aidesys::alert(std::isnan(precision), "illegal-argument", "in symbolingfield::InequalityType::InequalityType 'ntype' precision parameter undefined or ill-defined");
    distance = parameters.get("distance", 0.0);
    // Sets the bounds
    bounds = ntype.getBounds();
    bounds_count = bounds.length();
  }
  InequalityType::InequalityType(String parameters) : InequalityType(wjson::string2json(parameters))
  {}
  InequalityType::InequalityType(const char *parameters) : InequalityType(wjson::string2json(parameters))
  {}
  wjson::Value InequalityType::getValue(JSON value, bool semantically_else_syntactically) const
  {
    wjson::Value result = value;
    double l = value.get(name_left, zero), r = value.get(name_right, zero);
    double d = (l >= r ? 0.5 : -0.5) * distance, m = 0.5 * (l + r);
    result.set(name_left, m + d), result.set(name_right, m - d);
    return result;
  }
  double InequalityType::getDistance(JSON lhs, JSON rhs) const
  {
    if(Type::justDone(lhs, rhs)) {
      return Type::getDistance(lhs, rhs);
    }
    double dl = lhs.get(name_left, zero) - rhs.get(name_left, zero);
    double dr = lhs.get(name_right, zero) - rhs.get(name_right, zero);
    double distance = sqrt(dl * dl + dr * dr);
    Type::setDistance(lhs, rhs, distance);
    return distance;
  }
  wjson::Value InequalityType::getPath(JSON lhs, JSON rhs) const
  {
    if(!Type::justDone(lhs, rhs)) {
      InequalityType::getDistance(lhs, rhs);
    }
    double ll = lhs.get(name_left, zero), lr = lhs.get(name_right, zero), rl = rhs.get(name_left, zero), rr = lhs.get(name_right, zero);
    wjson::Value path, v = lhs;
    for(double e = 0; e <= 1; e = std::min(1.0, e + precision)) {
      v.set(name_left, ll + e * (rl - ll)), v.set(name_right, lr + e * (rr - lr));
      path.add(v);
    }
    return path;
  }
  String InequalityType::asString()
  {
    static std::string string;
    string = Type::asString();
    string = string.substr(0, string.length() - 1) + " parameters: " + getParameters().asString() + "}";
    return string;
  }
}
